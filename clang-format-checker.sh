#!/bin/bash

set -eo pipefail

NoColor="\033[0m"
CyanColor="\033[0;96m"
RedColor="\033[0;91m"

if [[ "$*" == *"-h"* ]] || [[ "$*" == *"--help"* ]]; then
    echo -e "Usage: $0 [Options]\n"
    echo "Options:"
    echo "  -r, --replace       Automatically fix formatting and modify files according the configuration. Not suitable with -e."
    echo "  -e, --export FILE   Export code with needed changements applied."
    exit 0
fi

clangFormatPath=$( which clang-format )
if [ ! $? -eq 0 ]; then
    echo -e "\n${RedColor}Please install clang-format first${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using clang-format at $cmakePath${NoColor}"
fi
clangTidyPath=$( which clang-tidy )
if [ ! $? -eq 0 ]; then
    echo -e "\n${RedColor}Please install clang-tidy first${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using clang-tidy at $cmakePath${NoColor}"
fi

files=`find . -name "*.cpp" -not -path "./build/*" -not -path "./deps/*" -o -name "*.hpp" -not -path "./build/*" -not -path "./deps/*"`
if [[ -z $files ]]; then
    echo -e "\n${RedColor}No file to check${NoColor}"
    exit 0
fi

if [[ "$*" == *"-e"* ]] || [[ "$*" == *"--export"* ]]; then
    for (( arg=1; arg<=$#; arg++ )); do
        nextarg=$((arg+1))
        if [[ ${!arg} == "-e" ]] || [[ ${!arg} == "--export" ]]; then
            filename=${!nextarg}
            break
        fi
    done
    if [[ -z $filename ]] || [[ ${filename::1} == '-' ]]; then
        echo -e "\n${RedColor}Error: '$filename' Invalid filename${NoColor}"
        exit 1
    fi

    clang-format $files > $filename

    echo -e "\n${CyanColor}Successfully exported clang-format report in '$filename'${NoColor}"
else
    if [[ "$*" == *"-r"* ]] || [[ "$*" == *"--replace"* ]]; then
        clang-format -i $files

        echo -e "\n${CyanColor}Your files have been automatically formatted${NoColor}"
    else
        clang-format $files
    fi
fi
