#ifdef USE_TEXT_MODULE

#include "GameEngine/ECS/Components/RenderableText.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/ECSErrors.hpp"
#include "GameEngine/ResourcesManager.hpp"
#include "fmt/core.h"

ge::ecs::component::RenderableText::RenderableText(std::string_view content, bool centered, size_t size, std::string_view fontPath)
{
    this->setFont(fontPath);
    this->setSize(size);
    this->setColor(sf::Color::White);
    this->setContent(content, centered);
}

ge::ecs::component::RenderableText::RenderableText(RenderableText&& other) noexcept :
    text(std::move(other.text))
{
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setContent(std::string_view content, bool centered) noexcept
{
    this->text.setString(static_cast<std::string>(content));

    if (centered) {
        auto size = this->text.getLocalBounds();
        this->text.setOrigin({size.width / 2, size.height / 2});
    } else {
        this->text.setOrigin({0, 0});
    }

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setSize(size_t size) noexcept
{
    this->text.setCharacterSize(size);

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setFont(std::string_view fontPath)
{
    try {
        this->text.setFont(ge::ResourcesManager::getFont(std::string(fontPath)));
    } catch (ge::error::ResourceError const& e) {
        throw error::ComponentError("RenderableText", fmt::format("Failed to create graphic object: '{}'", e.what()));
    }

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setColor(sf::Color color) noexcept
{
    this->text.setFillColor(color);

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setPosition(utils::Vector position) noexcept
{
    this->text.setPosition(position);

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setScale(utils::Vector scale) noexcept
{
    this->text.setScale(scale);

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::setRotation(float angle) noexcept
{
    this->text.setRotation(sf::degrees(angle));

    return *this;
}

ge::ecs::component::RenderableText& ge::ecs::component::RenderableText::draw(sf::RenderWindow& window) noexcept
{
    window.draw(this->text);

    return *this;
}

sf::FloatRect ge::ecs::component::RenderableText::getBounds() const noexcept
{
    return this->text.getGlobalBounds();
}

sf::Color ge::ecs::component::RenderableText::getColor() const noexcept
{
    return this->text.getFillColor();
}

void ge::ecs::component::RenderableText::hide(bool hide) noexcept
{
    this->hidden = hide;
}

bool ge::ecs::component::RenderableText::isHidden() const noexcept
{
    return this->hidden;
}

#endif
