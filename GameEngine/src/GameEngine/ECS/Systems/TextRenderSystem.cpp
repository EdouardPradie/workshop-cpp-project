#ifdef USE_TEXT_MODULE

#include "GameEngine/ECS/Systems/TextRenderSystem.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/RenderableText.hpp"

#include <iostream>

ge::ecs::system::TextRenderSystem::TextRenderSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::TextRenderSystem::update(Window& window)
{
    for (auto const& entity : this->entities) {
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& text = this->coordinator.getComponent<ecs::component::RenderableText>(entity);

        text.setPosition(attributes.position);
        text.setScale(attributes.scale);
        text.setRotation(attributes.angle);

        if (not text.isHidden()) {
            text.draw(window.window);
        }
    }
}

#endif
