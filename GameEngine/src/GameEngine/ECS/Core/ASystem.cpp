#include "GameEngine/ECS/Core/ASystem.hpp"

ge::ecs::core::ASystem::ASystem(Coordinator& coordinator) noexcept :
    coordinator(coordinator)
{
}

void ge::ecs::core::ASystem::addEntity(Entity const& entity) noexcept
{
    if (std::find(this->entities.begin(), this->entities.end(), std::size_t{entity}) == this->entities.end()) {
        this->entities.emplace_back(std::size_t{entity});
    }
}

void ge::ecs::core::ASystem::removeEntity(Entity const& entity) noexcept
{
    auto found = std::find(this->entities.begin(), this->entities.end(), std::size_t{entity});
    if (found != this->entities.end()) {
        this->entities.erase(found);
    }
}
