#ifdef USE_AUDIO_MODULE

#include "GameEngine/Audio/Music.hpp"

#include "GameEngine/ResourcesManager.hpp"
#include "GameEngine/Utils/FilePaths.hpp"
#include "fmt/core.h"

ge::audio::Music::Music(std::string filepath)
{
    filepath = fmt::format("{}/musics/{}", ASSETSPATH, filepath);
    if (not this->music.openFromFile(filepath)) {
        throw error::ResourceError(fmt::format("Invalid music file: {}", filepath));
    }
}

ge::audio::Music::~Music() noexcept
{
    this->stop();
}

void ge::audio::Music::play() noexcept
{
    this->music.play();
}

void ge::audio::Music::restart() noexcept
{
    this->music.stop();
    this->music.play();
}

void ge::audio::Music::pause() noexcept
{
    this->music.pause();
}

void ge::audio::Music::stop() noexcept
{
    this->music.stop();
}

void ge::audio::Music::setVolume(float volume) noexcept
{
    this->music.setVolume(volume);
}

void ge::audio::Music::setLoop(bool looping) noexcept
{
    this->music.setLoop(looping);
}

#endif
