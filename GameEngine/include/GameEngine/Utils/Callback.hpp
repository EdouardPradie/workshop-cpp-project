/**
 * @file Callback.hpp
 * @brief Function callback creator
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <functional>
#include <iostream>
#include <memory>
#include <tuple>

namespace ge::utils {

template <typename Sig, typename F, typename... Args>
struct CallbackMaker;

/**
 * @brief Template function and arguments types
 *
 * @tparam Ret The return type of the function
 * @tparam Args2 The call time given arguments types
 * @tparam F The function pointer type
 * @tparam Args The function pre captured arguments types
 */
template <typename Ret, typename... Args2, typename F, typename... Args>
/**
 * @struct CallbackMaker
 * @brief Callback creator structure
 */
struct CallbackMaker<Ret(Args2...), F, Args...> {
    /**
     * @brief Create a new callback objectj
     *
     * @param f The function pointer
     * @param args The arguments to pre-capture and pass first in the function arguments
     *
     * @return The function object
     */
    static std::function<Ret(Args2...)> createCallback(F f, Args&&... args)
    {
        return [f = std::move(f), ... args = std::forward<Args>(args)](Args2... args2) mutable {
            return std::invoke(f, std::forward<Args>(args)..., std::forward<Args2>(args2)...);
        };
    }
};

/**
 * @brief Template function and arguments types
 *
 * @tparam Sig The signature of the function without pre captured arguments(Args)
 * @tparam F The function pointer type
 * @tparam Args The function pre captured arguments types
 */
template <typename Sig, typename F, typename... Args>
/**
 * @brief Create a new callback object
 *
 * @param f The function pointer
 * @param args The function arguemnts
 *
 * @return The function object
 */
std::function<Sig> createCallback(F f, Args&&... args)
{
    return CallbackMaker<Sig, F, Args...>::createCallback(std::move(f), std::forward<Args>(args)...);
}

template <typename Sig = void()>
class Callback;

/**
 * @brief Template function callback
 *
 * @tparam Ret The return type of the callback
 * @tparam Args2 The types of the arguemnts given to the function at call time
 */
template <typename Ret, typename... Args2>
/**
 * @class Callback
 * @brief Callback object
 */
class Callback<Ret(Args2...)> {
   public:
    Callback() noexcept = default;
    /**
     * @brief Template a callback creation
     *
     * @tparam F The function pointer type
     * @tparam Args The type of the arguments captured by the callback and given first to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Create a new callback object
     *
     * @param f The function pointer
     * @param args The arguments to capture and pass first to the function pointer
     */
    explicit Callback(F&& f, Args&&... args) noexcept
        :
        f(CallbackMaker<Ret(Args2...), F, Args...>::createCallback(
            std::forward<F>(f), std::forward<Args>(args)...)),
        d(true)
    {
    }
    Callback(Callback const& other) noexcept = delete;
    Callback(Callback&& other) noexcept = default;
    ~Callback() noexcept = default;

    Callback& operator=(Callback const& other) noexcept = delete;
    Callback& operator=(Callback&& other) noexcept = default;

    /**
     * @brief Call the function
     * @details Note that this arguments will be passed after the previously captured arguments (inside the constructor)
     *
     * @param args The arguments to pass to the callback
     *
     * @return The callback return value
     *
     * @throw What the callback could throw
     * @throw `std::bad_function_call` if the callback is not defined by a function pointer
     */
    Ret operator()(Args2&&... args)
    {
        return this->f(std::forward<Args2>(args)...);
    }

    /**
     * @brief Call the function safely
     * @details Note that this arguments will be passed after the previously captured arguments (inside the constructor)
     *
     * @param args The arguments to pass to the callback
     *
     * @throw Anything the callback could throw
     */
    void safeRun(Args2&&... args)
    {
        if (this->f != nullptr) {
            this->f(std::forward<Args2>(args)...);
        }
    }
    /**
     * @brief Check if the callback is defined and associated to a function pointer
     *
     * @return `true` if the callback is defined by a function pointer, `false` otherwise
     */
    bool isDefined() const noexcept
    {
        return (this->f != nullptr);
    }
    /**
     * @brief Clear the stored function pointer
     */
    void clear() noexcept
    {
        this->f = {};
    }
    /**
     * @brief Template a callback creation
     *
     * @tparam F The function pointer type
     * @tparam Args The type of the arguments captured by the callback and given first to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Set the function pointer that define the callback
     *
     * @param f The function pointer
     * @param args The arguments to capture and pass first to the function pointer
     */
    void define(F&& f, Args&&... args) noexcept
    {
        this->f = CallbackMaker<Ret(Args2...), F, Args...>::createCallback(std::forward<F>(f), std::forward<Args>(args)...);
    }

   private:
    /**
     * @var f
     * @brief The lambda function to call that will itself call the function pointer
     */
    std::function<Ret(Args2...)> f{};
    bool d = false;
};

} // namespace ge::utils
