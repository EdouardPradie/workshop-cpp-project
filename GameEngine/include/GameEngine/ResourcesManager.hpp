/**
 * @file ResourceManager.hpp
 * @brief Manage graphic resources for optimization
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/Audio/SoundBuffer.hpp"
#include "SFML/Graphics/Font.hpp"
#include "SFML/Graphics/Texture.hpp"
#include "Utils/Singleton.hpp"

#include <exception>
#include <string>
#include <unordered_map>

namespace ge {

namespace error {

    class ResourceError : public std::exception {
       public:
        ResourceError() noexcept = delete;
        /**
         * @brief Generate an object to throw when an error occurs in font
         *
         * @param message A message that describe the error
         */
        explicit ResourceError(std::string const& message);
        ResourceError(ResourceError const& other) = default;
        ResourceError(ResourceError&& other) = default;
        ~ResourceError() override = default;

        ResourceError& operator=(ResourceError const& other) = default;
        ResourceError& operator=(ResourceError&& other) = default;

        /**
         * @brief Get a description of the error
         *
         * @return The error message defined in constructor
         */
        const char* what() const noexcept override;

       protected:
        /**
         * @var message
         * @brief Store the message given in the constructor that describe the error
         */
        std::string message;
    };

} // namespace error

/**
 * @class ResourceManager
 * @brief Manage graphic resources for optimization
 */
class ResourcesManager final : public utils::Singleton<ResourcesManager> {
   public:
    ResourcesManager() noexcept = delete;
    explicit ResourcesManager(token t) noexcept :
        Singleton(t) { }
    ResourcesManager(ResourcesManager const& other) noexcept = delete;
    ResourcesManager(ResourcesManager&& other) noexcept = delete;
    ~ResourcesManager() noexcept = default;

    ResourcesManager& operator=(ResourcesManager const& other) noexcept = delete;
    ResourcesManager& operator=(ResourcesManager&& other) noexcept = delete;

    /**
     * @brief Get a graphic font
     *
     * @param filepath The path of the file to load the font from
     *
     * @return A reference to the font
     *
     * @throw FontError thrown if the given file is invalid
     */
    static sf::Font& getFont(std::string filepath);
    /**
     * @brief Get a graphic texture
     *
     * @param filepath The path of the file to load the texture from
     *
     * @return A reference to the texture
     *
     * @throw TextureError thrown if the given file is invalid
     */
    static sf::Texture& getTexture(std::string filepath);
    /**
     * @brief Get a sound buffer
     *
     * @param filepath The path of the file to load the sound from
     *
     * @return A reference to the sound buffer
     *
     * @throw SoundError thrown if the given file is invalid
     */
    static sf::SoundBuffer& getSoundBuffer(std::string filepath);

   private:
    /**
     * @var fonts
     * @brief The list of loaded fonts
     */
    std::unordered_map<std::string, sf::Font> fonts;
    /**
     * @var textures
     * @brief The list of loaded textures
     */
    std::unordered_map<std::string, sf::Texture> textures;
    /**
     * @var sounds
     * @brief The list of loaded sounds
     */
    std::unordered_map<std::string, sf::SoundBuffer> sounds;
};

} // namespace ge
