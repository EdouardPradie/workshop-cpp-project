/**
 * @file Bounds.hpp
 * @brief Atributes component to set bounds Entity
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/Utils/Vector.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

#pragma once

namespace ge::ecs::component {
/**
 * @var Bounds
 * @brief Bounds is rectangle define bounds of Entity
 */
struct Bounds {
    /**
     * @var bounds
     * @brief rect of bounds
     */
    sf::FloatRect bounds;
    /**
     * @var transform
     * @brief transform of bounds
     */
    utils::Vector transform;
};

} // namespace ge::ecs::component
