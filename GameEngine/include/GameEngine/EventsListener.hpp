/**
 * @file EventsListener
 * @brief Listen for events that occurs in the graphic window
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "EventsKeyCodes.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/Window/Keyboard.hpp"

#include <unordered_map>
#include <vector>

namespace ge {

class EventsListener {
   public:
    EventsListener() noexcept = default;
    EventsListener(EventsListener const& other) noexcept = delete;
    EventsListener(EventsListener&& other) noexcept = delete;
    ~EventsListener() noexcept = default;

    EventsListener& operator=(EventsListener const& other) noexcept = delete;
    EventsListener& operator=(EventsListener&& other) noexcept = delete;

    /**
     * @brief getEvents
     *
     * @param window The window to get events from
     *
     * @return A vector of the different events that occured in the window since the last call to this function
     */
    std::vector<ge::Event> getEvents(sf::RenderWindow& window) const noexcept;
};

} // namespace ge
