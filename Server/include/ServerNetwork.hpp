/**
 * @file ServerNetwork.hpp
 * @brief The main class for server network
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "NetworkError.hpp"
#include "NetworkSerialiser.hpp"
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/System/Vector2.hpp"
#include "utils/Clock.hpp"

#include <SFML/Network.hpp>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <vector>

/**
 * @brief Byte(8 Bit) on binary level
 */
using Byte = std::uint8_t;
/**
 * @namespace Network
 * @brief This is the main namespace for all the network in this RTypes project.
 */

namespace network {
/**
 * @class ServerNetwork
 * @brief The main class for our network
 */
class ServerNetwork {

   public:
    ServerNetwork() noexcept = delete;
    /**
     * @fn ServerNetwork
     * @brief the constructor of ServerNetwork
     * @param serverPort The port value for the server
     * @warning serverPort need to be in this range (1-65535);
     * @warning When created you need to call the startNetwork method to start the server
     */
    explicit ServerNetwork(unsigned short serverPort);
    ServerNetwork(ServerNetwork const& other) noexcept = delete;
    ServerNetwork(ServerNetwork&& other) noexcept = delete;
    ServerNetwork& operator=(ServerNetwork const& other) noexcept = delete;
    ServerNetwork& operator=(ServerNetwork&& other) noexcept = delete;
    ~ServerNetwork() noexcept = default;

    /**
     * @fn startNetwork
     * @brief this function start the multithread server
     * @warning When we start this function your program will lock when whe don't have 4 connections
     */
    void startNetwork();
    /**
     * @fn updatePlayer
     * @brief update the data of the player
     * @param id the unique player id
     * @param pos the new position x and y of the player
     * @param life the life of the player
     * @param damage the damage of the player
     * @details this function will automaticaly add the new player data in queue before sending
     */
    void updatePlayer(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    /**
     * @fn updateEnemy
     * @brief update the data of the Enemy
     * @param id the unique enemy id
     * @param pos the new position x and y of the enemy
     * @param life the life of the enemy
     * @param damage the damage of the enemy
     * @details this function will automaticaly add the new enemy data in queue before sending
     */
    void updateEnemy(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    /**
     * @fn updateProjectile
     * @brief update the data of the projectile
     * @param id the unique projectile id
     * @param pos the new position x and y of the projectile
     * @param damage the damage of the projectile
     * @details this function will automaticaly add the new enemy data in queue before sending
     */
    void updateProjectile(unsigned short id, sf::Vector2f pos, unsigned short damage) noexcept;
    /**
     * @fn createPlayer
     * @brief Create the player and send it to the server
     * @param player the playerInfo data
     */
    void createPlayer(playerInfo& player) noexcept;
    /**
     * @fn createEnemy
     * @brief Create the enemyInfo and send it to the server
     * @param enemy the enemyInfo data
     */
    void createEnemy(enemyInfo& enemy) noexcept;
    /**
     * @fn createProjectile
     * @brief Create the projectile and send it to the server
     * @param projec the projectile data
     */
    void createProjectile(projectile& projec) noexcept;
    /**
     * @fn deleteEnemy
     * @brief delete the enemy passed as parameter
     * @param enemy the enemyInfo data
     */
    void deleteEnemy(enemyInfo& enemy) noexcept;
    /**
     * @fn deleteProjectile
     * @brief delete the projectile passed as parameter
     * @param projec the projectile data
     */
    void deleteProjectile(projectile& projec) noexcept;
    /**
     * @fn eraseDeadEnemy
     * @brief Remove the dead enemy from the network dataGrame trame
     * @param rowData the binary DataGramme data
     * @return the number of Byte removed
     */
    std::size_t eraseDeadEnemy(std::vector<Byte>& rowData);
    /**
     * @fn setRunning
     * @brief this function will allow to stop all the tread created by startNetwork
     * @param value True or False
     */
    void setRunning(bool value) noexcept;

   private:
    /**
     * @var socketReceive
     * @brief The udp socket for receive data on port passed as parameter when created the object
     */
    sf::UdpSocket socketReceive;
    /**
     * @var socketSend
     * @brief the send Socket
     */
    sf::UdpSocket socketSend;
    /**
     * @var NetworkSerialiser
     * @brief the Serializer for the network
     */
    NetworkSerialiser networkSerialiser;
    /**
     * @fn waitingRoom
     * @brief This function wait and lock the current thread unless we don't have 4 players
     */
    void waitingRoom();
    /**
     * @fn sendPlayerID
     * @brief send the unique player id of eatch client
     */
    void sendPlayerID() noexcept;
    /**
     * @fn sendDataToAllClient
     * @brief this function send the std::vector<Byte> dataGramme at all the players
     */
    void sendDataToAllClient() noexcept;
    /**
     * @fn sendLoop
     * @brief This function is the send loop for the thread
     */
    void sendLoop() noexcept;
    /**
     * @fn receiveData
     * @brief This function is the receive loop for the thread
     */
    void receiveData() noexcept;
    /**
     * @var playerMutex
     * @brief the mutex for the player
     */
    std::mutex playerMutex;
    /**
     * @var players
     * @brief the vector with all the playerInfo datas
     */
    std::vector<playerInfo> players;
    /**
     * @var playersConnection
     * @brief vector with all the playerConnection datas
     */
    std::vector<playerConnection> playersConnection;
    /**
     * @var enemyMutex
     * @brief The mutex for the enemys
     */
    std::mutex enemyMutex;
    /**
     * @var enemys
     * @brief vector with all the enemys
     */
    std::vector<enemyInfo> enemys;
    /**
     * @var projectMutex
     * @brief the mutex for the projectile
     */
    std::mutex projectMutex;
    /**
     * @var projectiles
     * @brief vector with all the projectiles datas
     */
    std::vector<projectile> projectiles;
    /**
     * @var sendClock
     * @brief the clock for sending data over network
     */
    utils::Clock sendClock;
    /**
     * @var receiveClock
     * @brief the clock for received data over network
     */
    utils::Clock receiveClock;
    /**
     * @var dataGrammeMutex
     * @brief the mutex for the dataGramme
     */
    std::mutex dataGrammeMutex;
    /**
     * @var dataGrame
     * @brief the vector of Byte
     */
    std::vector<Byte> dataGramme;
    /**
     * @var sendThread
     * @brief the join thread for sending data
     */
    std::jthread sendThread;
    /**
     * @var receiveThread
     * @brief the join thread for received data
     */
    std::jthread receiveThread;
    /**
     * @var running
     * @brief if the thread will run or not
     */
    bool running = true;
};

} // namespace network
