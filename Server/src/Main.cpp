#include "BinaryConverter.hpp"
#include "NetworkSerialiser.hpp"
#include "Randomizer.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/Network/Socket.hpp"
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/System/Vector2.hpp"
#include "ServerNetwork.hpp"
#include "utils/Clock.hpp"

#include <array>
#include <cstddef>
#include <iostream>
#include <mutex>
#include <optional>
#include <ostream>
#include <string>
#include <thread>

using Byte = std::uint8_t;

unsigned short getUniqueEnemyId()
{
    static unsigned short id = 3;
    id++;

    if (id > 50000) {
        id = 4;
    }

    return id;
}

void spawnEnemy(network::ServerNetwork& server)
{
    enemyInfo enemy;

    enemy.id = getUniqueEnemyId();
    enemy.pos.x = 1800;
    enemy.pos.y = static_cast<float>(utils::Randomizer::getRandomNumber(100, 980));
    enemy.enemyTypeID = static_cast<unsigned short>(utils::Randomizer::getRandomNumber(0, 0));
    server.createEnemy(enemy);
}

void gameLoop(network::ServerNetwork& server)
{
    utils::Clock clock;
    clock.saveTimePoint();
    int timeBetweenSpawn = 1000 * 4;

    while (true) {

        if (clock.isElapsed(timeBetweenSpawn)) {
            spawnEnemy(server);
            clock.saveTimePoint();
        }
    }
}

int main(int /*ac*/, char const* /*av*/[])
{
    unsigned short port = 55555;

    std::cout << "Local ip: " << sf::IpAddress::getLocalAddress().value() << std::endl;
    std::cout << "Public ip: " << sf::IpAddress::getPublicAddress().value() << std::endl;
    std::cout << "Port: " << port << std::endl;

    network::ServerNetwork server(port);
    server.startNetwork();
    gameLoop(server);
    return 0;
}

