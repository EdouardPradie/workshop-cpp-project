#include "ServerNetwork.hpp"

#include "Offset.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/Network/Socket.hpp"

#include <algorithm>
#include <mutex>
#include <thread>
#include <vector>

network::ServerNetwork::ServerNetwork(unsigned short serverPort)
{

    if (this->socketReceive.bind(serverPort) != sf::Socket::Status::Done) {
        throw network::error::NetworkError("Can't bind the socket port maybe already use.");
    }
    if (this->socketSend.bind(this->socketSend.getLocalPort()) != sf::Socket::Status::Done) {
        throw network::error::NetworkError("Can't bind the socket port for send maybe already use.");
    }

    this->players.resize(4);
    this->playersConnection.resize(4);
}

void network::ServerNetwork::waitingRoom()
{
    char temp[1024];
    std::size_t dataSizeGet = 0;

    for (int i = 0; i != 4; i++) {
        if (this->socketReceive.receive(temp, sizeof(temp), dataSizeGet, this->playersConnection[i].clientIp, this->playersConnection[i].clientPort) != sf::Socket::Status::Done) {
            i--;
            continue;
        }
        this->players[i].id = i;
        this->playersConnection[i].id = i;
        std::cout << "New client" << std::endl;
    }
}

void network::ServerNetwork::sendPlayerID() noexcept
{
    std::unique_lock<std::mutex> const lcx(this->playerMutex);

    for (int i = 0; i != this->players.size(); ++i) {
        // NOLINTNEXTLINE
        if (this->playersConnection[i].clientIp.has_value() && (this->socketSend.send(&this->players[i].id, sizeof(this->players[i].id), this->playersConnection[i].clientIp.value(), this->playersConnection[i].clientPort)) != sf::Socket::Status::Done) {
            i--;
            continue;
        }
    }
}

void network::ServerNetwork::updatePlayer(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage) noexcept
{

    std::unique_lock<std::mutex> const ltx(this->playerMutex);

    std::vector<Byte> playerData{};

    for (auto& player : this->players) {

        if (player.id == id) {
            player.pos = pos;
            player.life = life;
            player.damage = damage;
            playerData = this->networkSerialiser.convertPlayerToBinary(player);
        }
    }
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    this->dataGramme.insert(this->dataGramme.end(), playerData.begin(), playerData.end());
}

void network::ServerNetwork::updateEnemy(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage) noexcept
{

    std::unique_lock<std::mutex> const ltx(this->enemyMutex);

    std::vector<Byte> enemyData{};

    for (auto& enemy : this->enemys) {

        if (enemy.id == id) {
            enemy.pos = pos;
            enemy.life = life;
            enemy.damage = damage;
            enemyData = this->networkSerialiser.convertEnemyToBinary(enemy);
        }
    }
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    this->dataGramme.insert(this->dataGramme.end(), enemyData.begin(), enemyData.end());
}

void network::ServerNetwork::updateProjectile(unsigned short id, sf::Vector2f pos, unsigned short damage) noexcept
{
    std::unique_lock<std::mutex> const ltx(this->projectMutex);
    std::vector<Byte> projectileData{};

    for (auto& projectile : this->projectiles) {

        if (projectile.id == id) {
            projectile.pos = pos;
            projectile.damage = damage;
            projectileData = this->networkSerialiser.convertProjectileToBinary(projectile);
            break;
        }
    }
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    this->dataGramme.insert(this->dataGramme.end(), projectileData.begin(), projectileData.end());
}

void network::ServerNetwork::sendDataToAllClient() noexcept
{

    for (int i = 0; i != this->playersConnection.size(); i++) {

        // NOLINTNEXTLINE
        if (this->playersConnection[i].clientIp.has_value() && this->socketSend.send(this->dataGramme.data(), this->dataGramme.size(), this->playersConnection[i].clientIp.value(), this->playersConnection[i].clientPort) != sf::Socket::Status::Done) {
            i--;
            continue;
        }
    }
    this->dataGramme.clear();
}

void network::ServerNetwork::createPlayer(playerInfo& player) noexcept
{
    {

        std::unique_lock<std::mutex> ltx(this->playerMutex);
        this->players.push_back(player);
    }

    {
        std::vector<Byte> playerData = this->networkSerialiser.convertPlayerToBinary(player);
        std::unique_lock<std::mutex> ltx(this->dataGrammeMutex);
        this->dataGramme.insert(this->dataGramme.end(), playerData.begin(), playerData.end());
    }
}

void network::ServerNetwork::createEnemy(enemyInfo& enemy) noexcept
{
    {
        std::unique_lock<std::mutex> ltx(this->enemyMutex);
        this->enemys.push_back(enemy);
    }

    {
        std::vector<Byte> enemyData = this->networkSerialiser.convertEnemyToBinary(enemy);
        std::unique_lock<std::mutex> ltx(this->enemyMutex);
        this->dataGramme.insert(this->dataGramme.end(), enemyData.begin(), enemyData.end());
    }
}

void network::ServerNetwork::createProjectile(projectile& projec) noexcept
{
    {
        std::unique_lock<std::mutex> ltx(this->projectMutex);
        this->projectiles.push_back(projec);
    }

    {
        std::vector<Byte> projectileData = this->networkSerialiser.convertProjectileToBinary(projec);
        std::unique_lock<std::mutex> ltx(this->projectMutex);
        this->dataGramme.insert(this->dataGramme.end(), projectileData.begin(), projectileData.end());
    }
}

void network::ServerNetwork::deleteEnemy(enemyInfo& enemy) noexcept
{
    std::unique_lock<std::mutex> ltx(this->enemyMutex);

    for (int i = 0; i != this->enemys.size(); i++) {

        if (enemy.id == this->enemys[i].id) {
            this->enemys.erase(this->enemys.begin() + i);
            return;
        }
    }
}

void network::ServerNetwork::deleteProjectile(projectile& projec) noexcept
{
    std::unique_lock<std::mutex> ltx(this->projectMutex);

    for (int i = 0; i != this->projectiles.size(); i++) {

        if (projec.id == this->projectiles[i].id) {
            this->projectiles.erase(this->projectiles.begin() + i);
            return;
        }
    }
}

void network::ServerNetwork::sendLoop() noexcept
{
    this->sendPlayerID();
    this->sendClock.saveTimePoint();

    while (this->running) {
        std::unique_lock<std::mutex> ltx(this->dataGrammeMutex);
        if (!this->dataGramme.empty()) {
            this->sendDataToAllClient();
        }
    }
}

void network::ServerNetwork::receiveData() noexcept
{
    std::vector<Byte> rowData;
    rowData.resize(1500);
    std::size_t receivedSize = 0;
    std::optional<sf::IpAddress> receivedIp{};
    unsigned short receivedPort = 0;

    while (this->running) {

        if (this->socketReceive.receive(rowData.data(), 1500, receivedSize, receivedIp, receivedPort) != sf::Socket::Status::Done) {
            continue;
        }
        std::unique_lock<std::mutex> ltx(this->dataGrammeMutex);

        rowData.resize(receivedSize);
        this->dataGramme.insert(this->dataGramme.end(), rowData.begin(), rowData.end());
        rowData.clear();
        rowData.resize(1500);
    }
}

void network::ServerNetwork::startNetwork()
{
    this->waitingRoom();
    this->sendThread = std::jthread(&ServerNetwork::sendLoop, this);
    this->receiveThread = std::jthread(&ServerNetwork::receiveData, this);
}

void network::ServerNetwork::setRunning(bool value) noexcept
{
    this->running = value;
}
