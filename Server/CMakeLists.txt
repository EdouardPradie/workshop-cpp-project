cmake_minimum_required(VERSION 3.17)

project(r-type_server VERSION 0.0.1 LANGUAGES CXX)

###############################################################################
# CONFIGURATION
###############################################################################

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/../)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../cmake)

option(TESTING "Unit tests" OFF)

###############################################################################
# DEPENDENCIES
###############################################################################

###############################################################################
# SET COMPILATION FLAGS
###############################################################################
if (CMAKE_COMPILER_IS_GNUCXX)
	set(CMAKE_CXX_FLAGS_DEBUG "-Wall -Wextra -Wno-unused-parameter -W -g")
endif()
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3")

set(CMAKE_CXX_FLAGS_RELEASE "-O3")

###############################################################################
# SOURCES
###############################################################################
set(PROJECT_SOURCES
)
set(LIBRARIES
    pthread
    sfml-network
)
set(INCLUDES
    include/
)

###############################################################################
# CREATE EXECUTABLE
###############################################################################
add_executable(${PROJECT_NAME}
    src/Main.cpp
    src/ServerNetwork.cpp
    src/BinaryConverter.cpp
    src/Bitset.cpp
    src/NetworkError.cpp
    src/utils/Clock.cpp
    src/NetworkSerialiser.cpp
    ${PROJECT_SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
    ${INCLUDES}
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE
    ${LIBRARIES}
)

###############################################################################
# BUILD TESTS
###############################################################################

set(TESTS_NAME unit_tests_server)
set(TESTS_SOURCES
    tests/StartTests.cpp
    tests/TestTests.cpp
)
set(TESTS_LIBRARIES
    GTest::gtest_main
)

if (TESTING)
    add_executable(${TESTS_NAME}
        ${PROJECT_SOURCES}
        ${TESTS_SOURCES}
    )

    target_include_directories(${TESTS_NAME}
        PRIVATE
        ${INCLUDES}
    )

    target_link_libraries(${TESTS_NAME}
        PRIVATE
        ${LIBRARIES}
        ${TESTS_LIBRARIES}
    )

    include(FetchContent)
    FetchContent_Declare(
      googletest
      GIT_REPOSITORY https://github.com/google/googletest.git
      GIT_TAG release-1.12.1
    )

    # For Windows: Prevent overriding the parent project's compiler/linker settings
    set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

    FetchContent_MakeAvailable(googletest)

    include(GoogleTest)

    enable_testing()
    gtest_discover_tests(${TESTS_NAME})
endif()
