/**
 * @file Entity.hpp
 * @brief The ECS entity definition
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <cstddef>
#include <functional>

namespace ecs::core {

class Coordinator;

/**
 * @struct Entity
 * @brief The primary ECS object
 */
struct Entity {
   public:
    Entity() noexcept = delete;
    /**
     * @brief Create a new entity object
     *
     * @param id The entity id
     */
    explicit Entity(std::size_t id) noexcept;
    Entity(Entity const& other) noexcept = delete;
    Entity(Entity&& other) noexcept = default;
    ~Entity() noexcept = default;

    Entity& operator=(Entity const& other) noexcept = delete;
    Entity& operator=(Entity&& other) noexcept = delete;

    // NOLINTNEXTLINE
    operator std::size_t() const;

   private:
    /**
     * @brief The id that represent the entity
     */
    std::size_t id;
};

} // namespace ecs::core
