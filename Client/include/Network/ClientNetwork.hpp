/**
 * @file ClientNetwork.hpp
 * @brief The main class for server network
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "NetworkError.hpp"
#include "NetworkSerialiser.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/System/Vector2.hpp"
#include "Utils/Clock.hpp"

#include <SFML/Network.hpp>
#include <arpa/inet.h>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <vector>

using Byte = std::uint8_t;
/**
 * @namespace Network
 * @brief This is the main namespace for all the network in this RTypes project.
 */

namespace network {

class ClientNetwork {

   public:
    ClientNetwork() noexcept = delete;
    ClientNetwork(sf::IpAddress servIp, unsigned short& serverPort);
    ClientNetwork(ClientNetwork const& other) noexcept = delete;
    ClientNetwork(ClientNetwork&& other) noexcept = delete;
    ClientNetwork& operator=(ClientNetwork const& other) noexcept = delete;
    ClientNetwork& operator=(ClientNetwork&& other) noexcept = delete;
    ~ClientNetwork() noexcept = default;

    void startNetwork();

    void updatePlayer(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    void updateEnemy(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    void updateProjectile(unsigned short id, sf::Vector2f pos, unsigned short damage, unsigned short ownerId) noexcept;

    void createPlayer(playerInfo& player) noexcept;
    void createEnemy(enemyInfo& enemy) noexcept;
    void createProjectile(projectile& projec) noexcept;

    void deleteEnemy(unsigned short id) noexcept;
    void deleteProjectile(unsigned short id) noexcept;

    void sendToServer() noexcept;
    void receiveData() noexcept;

    const unsigned short getPlayerId() const noexcept;

    const std::vector<playerInfo>& getPlayersInfo() const noexcept;
    const std::vector<enemyInfo>& getEnemiesInfo() const noexcept;
    const std::vector<projectile>& getProjectilesInfo() const noexcept;

    void removeEnemy(const unsigned short id) noexcept;

    const bool isMyProjectile(const unsigned short) noexcept;
    void removeOfMyProjectile(const unsigned short id) noexcept;

    void setRunning(bool value) noexcept;

   private:
    sf::UdpSocket clientSocket;

    NetworkSerialiser networkSerialiser;

    std::mutex playerMutex;
    std::vector<playerInfo> players;

    std::mutex enemyMutex;
    std::vector<enemyInfo> enemys;

    std::mutex projectMutex;
    std::vector<projectile> projectiles;

    utils::Clock sendClock;

    std::vector<unsigned short> myProjectilesIds;

    std::mutex dataGrammeMutex;
    std::vector<Byte> dataGramme;

    std::jthread sendThread;
    std::jthread receiveThread;

    sf::IpAddress serverIp;
    unsigned short serverPort = 0;

    bool running = true;

    unsigned short playerID = 0;

    void receivePlayerId() noexcept;
    std::mutex playeridMutex;
};
}; // namespace network
