/**
 * @file Tick.hpp
 * @brief Tick Class for regulate the time.
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "Utils/Clock.hpp"

/**
 * @namespace network
 */
namespace network {

/**
 * @class Tick
 * @brief This is the tick class to create the game tick system
 */
class Tick {
   public:
    Tick() noexcept = delete;

    explicit Tick(int tickRate) noexcept;
    Tick(Tick const& other) noexcept = default;
    Tick(Tick&& other) noexcept = default;
    Tick& operator=(Tick const& other) noexcept = default;
    Tick& operator=(Tick&& other) noexcept = default;
    ~Tick() noexcept = default;

    /**
     * @brief Return if the current tick is done.
     *
     * @return bool
     *
     */
    bool isTime() noexcept;
    /**
     * @brief Start the tick System
     *
     * @return void
     */
    void start() noexcept;

   private:
    utils::Clock clock; /*!< Class clock for time gesiton */
    int tickRate{}; /*!< The number of tick in ms */
};
} // namespace network
