/**
 * @file DataGramme.hpp
 * @brief The storage of data before send with network
 * @author Benjamin
 * @version 1.1
 */

#pragma once

#include <cstdint>
#include <vector>

using Byte = std::uint8_t;

/**
 * @namespace network
 * @brief Network namespace for RType
 */

namespace network {

/**
 *
 * @class DataGramme
 * @brief This class store eatch data in binary before sending with network.
 * @warning You need to use the BinaryConverter class to change your variable in binary.
 */
class DataGramme {
   public:
    DataGramme() noexcept = default;
    DataGramme(DataGramme const& other) noexcept = default;
    DataGramme(DataGramme&& other) noexcept = default;
    DataGramme& operator=(DataGramme const& other) noexcept = default;
    DataGramme& operator=(DataGramme&& other) noexcept = default;
    ~DataGramme() noexcept = default;

    /**
     * @fn getBinaryData
     * @brief Getter for the private parameter binaryData.
     * @warning When you call this function the private variable importantData, normalData, lowData will be concat in binaryData. After thant this 3 variable (importantData, normalData, lowData) will be reset.
     *
     * @return The value of binaryData (the importantData in first place next to the normalData next to lowData).
     */
    std::vector<std::vector<std::vector<Byte>>> getBinaryData() noexcept;
    /**
     * @fn clearBinaryData
     * @brief Reset the private variable binaryData
     */
    void clearBinaryData() noexcept;
    /**
     * @fn clearAll
     * @brief Reset all the private variable.
     */
    void clearAll() noexcept;
    /**
     * @fn addImportantData
     * @brief Add a vector in the importantData private variable
     * @param data The data in binary format with signature.
     *
     */
    void addImportantData(const std::vector<Byte>& data) noexcept;
    /**
     * @fn getImportantData
     * @brief Getter for the private variable importantData
     *
     * @return The importantData private variable
     */
    std::vector<std::vector<Byte>> getImportantData() noexcept;
    /**
     * @fn addNormalData
     * @brief Add a vector in the normalData private variable
     * @param data The data in binary format with signature.
     */
    void addNormalData(const std::vector<Byte>& data) noexcept;
    /**
     * @fn getNormalData
     * @brief Getter for the private variable normalData.
     *
     * @return The normalData private variable.
     */
    std::vector<std::vector<Byte>> getNormalData() noexcept;
    /**
     * @fn addLowData
     * @brief Add a vector in the lowData private variable.
     * @param data The data in binary format with signature.
     */
    void addLowData(const std::vector<Byte>& data) noexcept;
    /**
     * @fn getLowData
     * @brief Getter for the private variable lowData
     *
     * @return The lowData private variable.
     */
    std::vector<std::vector<Byte>> getLowData() noexcept;

   private:
    /**
     * @var Store the binary data of the 3 priority in the good orders.
     */
    std::vector<std::vector<std::vector<Byte>>> binaryData;
    /**
     * @var Store the important binary data.
     */
    std::vector<std::vector<Byte>> importantData;
    /**
     * @var Store the normal binary data.
     */
    std::vector<std::vector<Byte>> normalData;
    /**
     * @var Store the low importance binary data.
     */
    std::vector<std::vector<Byte>> lowData;
};
} // namespace network
