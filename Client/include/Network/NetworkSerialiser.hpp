#pragma once

#include "BinaryConverter.hpp"
#include "ECS/Core/Bitset.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/System/Vector2.hpp"

#include <optional>
#include <vector>
using Byte = std::uint8_t;

struct playerConnection {
    std::optional<sf::IpAddress> clientIp;
    unsigned short clientPort = 0;
    unsigned short id = 0;
};
//-------------------------------------------------
// OFFSET OF 1
struct playerInfo {
    unsigned short id = 5000;
    sf::Vector2f pos{};
    unsigned short life = 100;
    unsigned short damage = 10;
}; // 16 Bytes

//------------------------------------------------
// OFFSET OF 2
struct enemyInfo {
    sf::Vector2f pos{};
    unsigned short life = 100;
    unsigned short damage = 10;
    unsigned short id = 0;
    unsigned short enemyTypeID = 0;
}; // 12 Bytes

// OFFSET OF 3
struct projectile {
    unsigned short damage = 101;
    sf::Vector2f pos{};
    unsigned short id = 0;
    unsigned short projectileTypeID = 0;
    unsigned short ownerId = 500;
};

namespace network {
class NetworkSerialiser {

   public:
    NetworkSerialiser() noexcept = default;
    NetworkSerialiser(NetworkSerialiser const& other) noexcept = default;
    NetworkSerialiser(NetworkSerialiser&& other) noexcept = default;
    NetworkSerialiser& operator=(NetworkSerialiser const& other) noexcept = default;
    NetworkSerialiser& operator=(NetworkSerialiser&& other) noexcept = default;
    ~NetworkSerialiser() noexcept = default;

    unsigned short getOffset(std::vector<Byte>& rowData) noexcept;

    std::vector<Byte> convertPlayerToBinary(struct playerInfo& player) noexcept;
    struct playerInfo convertBinaryToPlayer(std::vector<Byte>& rowData) noexcept;

    std::vector<Byte> convertEnemyToBinary(struct enemyInfo& enemy) noexcept;
    struct enemyInfo convertBinaryToEnemy(std::vector<Byte>& rowData) noexcept;

    std::vector<Byte> convertProjectileToBinary(struct projectile& projectile) noexcept;
    struct projectile convertBinaryToProjectile(std::vector<Byte>& rowData) noexcept;

    std::vector<Byte> addSignature(unsigned short OFFSET) noexcept;

   private:
    unsigned short dataSizeToRead = 0;
    ecs::core::Bitset bitset;
    BinaryConverter bconverter;
};
} // namespace network
