#include "Scenes/MenuScene.hpp"
#include "Scenes/GameScene.hpp"

#include "GameEngine/GameEngine.hpp"
#include "GameEngine/Utils/Vector.hpp"

scene::MenuScene::MenuScene() :
    text(ge::getCoordinator().createEntity()), howPlayText(ge::getCoordinator().createEntity()), hintText(ge::getCoordinator().createEntity()), background(ge::getCoordinator().createEntity())

{
    ge::getCoordinator().setComponent<ge::ecs::component::RenderableImage>(this->background, scene::MenuScene::backgroundPath, ge::utils::Vector{-1, -1}, false);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->background);
    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->text, "R-TYPE", true, 200);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->text, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.2}});
    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->howPlayText, "To move, use the arrows keys and to shoot use space", true, 60);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->howPlayText, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.6}});

    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->hintText, "Press Enter to start the game :3 UwU", true, 60);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->hintText, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.9}});
    
    ge::getEventsHandler().setPressedKeyCallback(ge::KeyboardKeys::Enter, &ge::GameEngine::setScene<scene::GameScene>);
}

scene::MenuScene::~MenuScene() noexcept
{
    ge::getEventsHandler().clearPressedKeyCallbackFor(ge::KeyboardKeys::Space);

    ge::getCoordinator().destroyEntity(this->text);
    ge::getCoordinator().destroyEntity(this->howPlayText);
    ge::getCoordinator().destroyEntity(this->hintText);
}

void scene::MenuScene::update(float elapsedTime)
{
}