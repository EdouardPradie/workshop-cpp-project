#include "Game/BorderWalls.hpp"

#include "ECS/Components/Area.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Collider.hpp"

static void setupWall(ecs::core::Coordinator& coordinator, ecs::core::Entity& entity, sf::Vector2f pos, sf::Vector2f size)
{
    coordinator.setComponent<ecs::component::Attributes>(entity, ecs::component::Attributes{.position = pos});
    coordinator.setComponent<ecs::component::Area>(entity, ecs::component::Area(size, false));
    coordinator.setComponent<ecs::component::Bounds>(entity);
    coordinator.setComponent<ecs::component::Collider>(entity, ecs::component::Collider(ecs::component::ColliderType::OBSTACLE));
}

game::BorderWalls::BorderWalls(ecs::core::Coordinator& coordinator) :
    upWall(coordinator.createEntity()), downWall(coordinator.createEntity()), leftWall(coordinator.createEntity()), rightWall(coordinator.createEntity())
{
    setupWall(coordinator, this->upWall, {0, -1}, {1920, 1});
    setupWall(coordinator, this->downWall, {0, 1081}, {1920, 1});
    setupWall(coordinator, this->leftWall, {-1, 0}, {1, 1080});
    setupWall(coordinator, this->rightWall, {1921, 0}, {1, 1080});
}
