#include "Audio/AudioError.hpp"

audio::error::AudioError::AudioError(std::string const& message) :
    message(message)
{
}

const char* audio::error::AudioError::what() const noexcept
{
    return this->message.c_str();
}

audio::error::MusicError::MusicError(std::string const& message) :
    AudioError(message)
{
}

const char* audio::error::MusicError::what() const noexcept
{
    return this->message.c_str();
}

audio::error::SoundError::SoundError(std::string const& message) :
    AudioError(message)
{
}

const char* audio::error::SoundError::what() const noexcept
{
    return this->message.c_str();
}
