
#include "Utils/Randomizer.hpp"

#include <ctime>

void utils::Randomizer::resetSeed() noexcept
{
    // NOLINTNEXTLINE
    srand(time(0));
}
