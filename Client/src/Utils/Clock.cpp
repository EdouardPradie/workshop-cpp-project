#include "Utils/Clock.hpp"

utils::Clock::Clock() noexcept { this->saveTimePoint(); }

void utils::Clock::saveTimePoint() noexcept
{
    this->timepoint = this->getActualTime();
}

bool utils::Clock::isElapsed(int milliseconds) const noexcept
{
    time_t const actualTime = this->getActualTime();

    return (this->timepoint + milliseconds <= actualTime);
}

float utils::Clock::getElapsedTime() const noexcept
{
    time_t const actualTime = this->getActualTime();

    return actualTime - this->timepoint;
}

#ifndef WINDOWS
time_t utils::Clock::getActualTime() noexcept
{
    struct timeval time_now {
    };

    gettimeofday(&time_now, nullptr);
    return ((time_now.tv_sec * 1000) + (time_now.tv_usec / 1000));
}
#else // windows
#include <windows.h>

time_t utils::Clock::getActualTime() noexcept
{
    SYSTEMTIME st;

    GetSystemTime(&st);
    return ((st.wSecond * 1000) + (st.wMilliseconds));
}
#endif // windows
